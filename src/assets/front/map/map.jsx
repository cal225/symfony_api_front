import './map.css';
import 'leaflet/dist/leaflet.css';

import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import React, { useState, useEffect } from 'react';

export function Map({ data }) {
        // State hooks for map center and refresh event
    const [mapCenter, setMapCenter] = useState([46.791301764317026, 6.2985654481172]);
    const [refreshEvent, setRefreshEvent] = useState(0);

    // Effect hook to update map center when data changes
    useEffect(() => {
        if (data) {
            setMapCenter([data[1], data[0]]);
        }
    }, [data]);

    // Effect hook to trigger re-rendering when data changes
    useEffect(() => {
        // Incrementing the refresh key will force a re-render of the MapContainer
        setRefreshEvent(prevEvent => prevEvent + 1);
    }, [data]); // Refresh whenever data changes

    return (
        <>

            <div className='left_side' >
                <div className='title'>
                    <h1>Mario's Memory Merchant</h1>
                    <p>©Mario Perreira</p>
                </div>


                <MapContainer
                    key={refreshEvent}
                    center={mapCenter}
                    zoom={8}
                    scrollWheelZoom={true}
                    className="leaflet-map-container"
                >
                    <TileLayer
                        attribution='Map data: &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
                        url="https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png"
                    />
                    <Marker position={mapCenter}> <Popup>{'lat=' + data[1] + ' long=' + data[0]}</Popup> </Marker>
                </MapContainer>

            </div>
        </>
    );
}
