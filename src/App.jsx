// ParentComponent.jsx
import React, { useState } from 'react';

import { Menu } from './assets/front/menu/menu.jsx';
import { Map } from './assets/front/map/map.jsx';

export default function ParentComponent() {
    const [data, setData] = useState('');

    return (
        <>
            <Menu setData={setData} />
            <Map data={data} />
        </>
    );
}