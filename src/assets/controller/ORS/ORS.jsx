// ORS.jsx

export const handleInputChange = (event, setInputValue) => {
    setInputValue(event.target.value);
};

export const getGeoCodeData = async (inputValue) => {

    const API_key = '5b3ce3597851110001cf62485ee868693c564f94a848e39b7b21d61f'; //will change location later

    // Call OpenRouteService API
    const response = await fetch(
        `https://api.openrouteservice.org/geocode/search?api_key=${API_key}&text=${encodeURIComponent(inputValue)}` // encodeURIComponent to prevent special characters
    );

    const data = await response.json();

    // Return latitude and longitude
    return data.features[0].geometry.coordinates;
};
