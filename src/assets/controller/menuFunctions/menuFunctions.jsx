// menuFunctions.js
import { getGeoCodeData } from '../ORS/ORS.jsx';

export const handleSubmit = async (event, inputValue, setData) => {
    event.preventDefault();
    const data = await getGeoCodeData(inputValue);
    setData(data);
};

export const handleInputChange = (event, setInputValue) => {
    setInputValue(event.target.value);
};
